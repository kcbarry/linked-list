#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include<exception>

using namespace std;

class noDataException : public exception{
	virtual const char* what() const throw(){
		return "No data found at node."; 
	}
}noData;

template<typename T>

struct Node{
	T data;
	Node<T> *next;
};

template <class T>
class LinkedList{
	private:
		Node<T> *head;
		unsigned int size; 
	public:
		LinkedList<T>(){
			head = NULL;
			size = 0;
		}
		
		T getDataAtIndex(int index){
			Node<T> *node = head;
			for(int i = 0; i < index;i++){
				node = node->next;
				if(!node){
					throw noData;
				}
			}
			return node->data;
		}
		
		void append(T data){
			if(!head){
				head = new Node<T>();
				head->data = data;
			}else{
				Node<T> *node = head;
				while(node->next){
					node = node->next;
				}
 				Node<T> *newNode = new Node<T>();
				newNode->data = data;
				node->next = newNode;
			}
			size++;
		}
		
		void insertAtIndex(T data,int index){
			if(index >= size){
				this->append(data);
			}else{
				Node<T> *newNode = new Node<T>();
				newNode->data = data;
				Node<T> *tempNode;
				if(index == 0){
 					tempNode = head;
 					newNode->next = tempNode;
 					head = newNode;
 				}else{
 					tempNode = head;
					for(int i = 0; i < index-1;i++){
						tempNode = tempNode->next;
					}
					newNode->next = tempNode->next;
					tempNode->next = newNode;
				}
				size++;
			}	
		}
		
		void removeFromIndex(int index){
			if(index < size){
				Node<T> *node;
				if(index == 0){
					node = head->next;
					delete head;
					head = node;
				}else{
					node = head;
					for(int i = 0; i < index-1;i++){
						node = node->next;
					}					
					Node<T> *nodeToDelete = node->next;
					node->next = nodeToDelete->next;
					delete nodeToDelete;
				}
				size--;
			}
		}
		
		void reverse(){
			Node<T> *node = head;
			Node<T> *temp = NULL;
			Node<T> *previous = NULL;
			while(node != NULL){
				temp = node->next;
				node->next = previous;
				previous = node;
				node = temp;
			}
			head = previous;
		}
		
		void print(){
			Node<T> *node = head;
			std::cout << "Begin List: " << std::endl;
			while(node){
				std::cout << "\t" << node->data << std::endl;
				node = node->next;
			}
			std::cout << "End List" << std::endl;	
		}
};





#endif
